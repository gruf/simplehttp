package main

import (
	"html/template"
	"io"
	"io/fs"
	"net/http"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"

	"codeberg.org/gruf/go-bytesize"
	"codeberg.org/gruf/go-glob"
	"codeberg.org/gruf/go-kv"
	"codeberg.org/gruf/go-mimetypes"
)

type FS struct {
	// pool is a memory pool of FSRequest objects.
	pool sync.Pool

	// IndexTemplate is the template to use when generating a dir index.
	IndexTemplate *template.Template

	// SortFiles is the set file sorting function for index page generation.
	SortFiles func([]file)

	// rootDir is the root path of this FS.
	RootDir string

	// IndexFiles are index files to look for when presented with directory path.
	IndexFiles []string

	// Restricted is a list of matching glob expressions of restricted paths.
	Restricted []*glob.GlobExpr

	// AcceptRange specifies whether to accept and handle range headers.
	AcceptRange bool

	// GenerateIndex specifies whether to generate dir index if no index file present.
	GenerateIndex bool
}

func (fs *FS) ServeHTTP(rw http.ResponseWriter, r *http.Request) {
	var err error

	// deferred file close.
	var close func() error

	// Acquire new FSRequest from pool
	fsreq := fs.pool.Get().(*FSRequest)
	defer func() {
		if close != nil {
			_ = close()
		}

		// Re-pool buffer.
		fsreq.Reset()
		fs.pool.Put(fsreq)
	}()

	// Get path param and check for restrictions
	fsreq.RawPath = fsreq.PBuf.Clean(r.URL.Path)
	if fs.restricted(fsreq.RawPath) {
		http.Error(rw, "Forbidden", http.StatusForbidden)
		return
	}

	// Build filesystem path from cleaned URL path
	fsreq.Path = fsreq.PBuf.Join(fs.RootDir, fsreq.RawPath)

	// Attempt to open file path
	fsreq.File, err = os.OpenFile(
		fsreq.Path,
		os.O_RDONLY,
		0o400,
	)

	switch {
	case err == nil:
		// No issue, ensure closes
		close = fsreq.File.Close

	case os.IsNotExist(err) || os.IsPermission(err):
		// Non-existent/permission errors return 404
		http.Error(rw, "File Not Found", http.StatusNotFound)
		return

	default:
		// All else, log and return generic error
		httputil.HTTPError(rw, r, "Error Opening File", http.StatusInternalServerError, err)
		return
	}

	// Retrieve file information
	fsreq.Stat, err = fsreq.File.Stat()
	if err != nil {
		httputil.HTTPError(rw, r, "Error Opening File", http.StatusInternalServerError, err)
		return
	}

	switch {
	case fsreq.Stat.Mode().IsRegular():
		// Pass to regular file handler
		fs.serveFile(rw, r, fsreq)
		return

	case fsreq.Stat.Mode().IsDir():
		// Look for index file or attempt index
		//
		// Redirect if doesn't end in '/'
		if !strings.HasSuffix(r.URL.Path, "/") {
			path := r.URL.Path + "/"
			http.Redirect(rw, r, path, http.StatusMovedPermanently)
			return
		}

	loop: // Look for index files, in dir
		for _, index := range fs.IndexFiles {
			// Build filesystem path for index file
			idxPath := fsreq.PBuf.Join(fsreq.Path, index)

			// Attempt to open file path
			file, err := os.OpenFile(
				idxPath,
				os.O_RDONLY,
				0o400,
			)

			switch {
			case err == nil:
				// No issue, ensure closes

			case os.IsNotExist(err) || os.IsPermission(err):
				// Non-existent/permission, keep looking
				continue loop

			default:
				// All else, log and return generic error
				httputil.HTTPError(rw, r, "Error Opening File", http.StatusInternalServerError, err)
				return
			}

			// Get the file stat
			stat, err := file.Stat()
			if err != nil {
				httputil.HTTPError(rw, r, "Error Opening File", http.StatusInternalServerError, err)
				_ = file.Close()
				return
			} else if stat.IsDir() {
				_ = file.Close()
				continue // ignore
			}

			// Update fsreq to new file
			_ = fsreq.File.Close()
			fsreq.File = file
			fsreq.Stat = stat
			fsreq.Path = idxPath

			// Set deferred close
			close = file.Close

			// Pass to file handler
			fs.serveFile(rw, r, fsreq)
			return
		}

		if !fs.GenerateIndex {
			// No index files found, and index generation
			// is disabled. Return a "not found" page.
			http.Error(rw, "File Not Found", http.StatusNotFound)
			return
		}

		// Serve a directory index
		fs.serveDir(rw, r, fsreq)
		return

	default:
		// All else, just use 'not found'
		http.Error(rw, "File Not Found", http.StatusNotFound)
	}
}

// serveFile will serve the file currently open in 'fsreq'.
func (fs *FS) serveFile(rw http.ResponseWriter, r *http.Request, fsreq *FSRequest) {
	var (
		n    int
		err  error
		mbuf []byte
	)

	// Attempt to get mimeType from file name
	mimeType, ok := mimetypes.GetForFilename(fsreq.File.Name())
	if !ok {
		// Get mimebuf from fsreq
		mbuf = fsreq.Buf.B[0:512]

		// Read first 512 bytes (file header)
		n, err = io.ReadFull(fsreq.File, mbuf)
		if err != nil && err != io.ErrUnexpectedEOF {
			http.Error(rw, "Error Reading File", http.StatusInternalServerError)
			return
		}

		// Detect mime type from read bytes
		mimeType = http.DetectContentType(mbuf[:n])
	}

	// Set determined content type header
	rw.Header().Set("Content-Type", mimeType)

	if fs.AcceptRange {
		// File ranges enabled, check for and pass to range handler
		rw.Header().Set("Accept-Ranges", "bytes")
		if rng := r.Header.Get("Range"); len(rng) > 0 {
			fs.serveFileRange(rw, r, fsreq, rng)
			return
		}
	}

	if n > 0 {
		// Mimebuf filled, write mimebuf contents to client
		if _, err := rw.Write(mbuf[:n]); err != nil {
			log.ErrorKVs(kv.Fields{
				{K: "id", V: getRequestID(r)},
				{K: "error", V: err},
				{K: "msg", V: "ResponseWriter.Write() error"},
			}...)
			return
		} else if n < 512 {
			return // short file
		}
	}

	// Set content-size and write the response body
	rw.Header().Set("Content-Length", strconv.FormatInt(fsreq.Stat.Size(), 10))
	if _, err := io.CopyBuffer(rw, fsreq.File, fsreq.Buf.Full()); err != nil {
		log.ErrorKVs(kv.Fields{
			{K: "id", V: getRequestID(r)},
			{K: "error", V: err},
			{K: "msg", V: "io.Copy() error"},
		}...)
		return
	}
}

// serveFileRange will serve a provided byte range (header value 'rng') for given filesystem request.
func (fs *FS) serveFileRange(rw http.ResponseWriter, r *http.Request, fsreq *FSRequest, rng string) {
	var i int

	if i = strings.IndexByte(rng, '='); i < 0 {
		// Range must include a separating '=' to indicate start
		http.Error(rw, "Bad Range Header", http.StatusBadRequest)
		return
	}

	if rng[:i] != "bytes" {
		// We only support byte ranges in our implementation
		http.Error(rw, "Unsupported Range Unit", http.StatusBadRequest)
		return
	}

	// Reslice past '='
	rng = rng[i+1:]

	if i = strings.IndexByte(rng, '-'); i < 0 {
		// Range header must contain a beginning and end separated by '-'
		http.Error(rw, "Bad Range Header", http.StatusBadRequest)
		return
	}

	var (
		err error

		// load the file's size
		size = fsreq.Stat.Size()

		// default start + end ranges
		start, end = int64(0), size - 1

		// start + end range strings
		startRng, endRng string
	)

	if startRng = rng[:i]; startRng != "" {
		// Parse the start of this byte range
		start, err = strconv.ParseInt(startRng, 10, 64)
		if err != nil {
			http.Error(rw, "Bad Range Header", http.StatusBadRequest)
			return
		}

		if start < 0 {
			// This range starts *before* the file start, why did they send this lol
			rw.Header().Set("Content-Range", "bytes *"+strconv.FormatInt(size, 10))
			http.Error(rw, "Unsatisfiable Range", http.StatusRequestedRangeNotSatisfiable)
			return
		}
	} else {
		// No start supplied, implying file start
		startRng = "0"
	}

	if endRng = rng[i+1:]; endRng != "" {
		// Parse the end of this byte range
		end, err = strconv.ParseInt(endRng, 10, 64)
		if err != nil {
			http.Error(rw, "Bad Range Header", http.StatusBadRequest)
			return
		}

		if end > size {
			// This range exceeds length of the file, therefore unsatisfiable
			rw.Header().Set("Content-Range", "bytes *"+strconv.FormatInt(size, 10))
			http.Error(rw, "Unsatisfiable Range", http.StatusRequestedRangeNotSatisfiable)
			return
		}
	} else {
		// No end supplied, implying file end
		endRng = strconv.FormatInt(end, 10)
	}

	if start >= end {
		// This range starts _after_ their range end, unsatisfiable and nonsense!
		rw.Header().Set("Content-Range", "bytes *"+strconv.FormatInt(size, 10))
		http.Error(rw, "Unsatisfiable Range", http.StatusRequestedRangeNotSatisfiable)
		return
	}

	// Set cursor to determined start position
	if _, err := fsreq.File.Seek(start, 0); err != nil {
		httputil.HTTPError(rw, r, "Error Reading File", http.StatusInternalServerError, err)
		return
	}

	// Cast to reader in case of wrapping
	rd := (io.Reader)(fsreq.File)

	// Determine new content length
	// after slicing to given range.
	length := end - start + 1

	if end < size-1 {
		// Range end < file end, limit the reader
		rd = io.LimitReader(rd, length)
	}

	// Write the necessary length and range headers
	rw.Header().Set("Content-Range", "bytes "+startRng+"-"+endRng+"/"+strconv.FormatInt(size, 10))
	rw.Header().Set("Content-Length", strconv.FormatInt(length, 10))
	rw.WriteHeader(http.StatusPartialContent)

	// Finally, write the response body
	if _, err := io.CopyBuffer(rw, rd, fsreq.Buf.Full()); err != nil {
		log.ErrorKVs(kv.Fields{
			{K: "id", V: getRequestID(r)},
			{K: "error", V: err},
			{K: "msg", V: "io.Copy() error"},
		}...)
		return
	}
}

// serveDir will generate and and serve an index page for the contents of directory in 'fsreq'.
func (fs *FS) serveDir(rw http.ResponseWriter, r *http.Request, fsreq *FSRequest) {
	// Read the directory entries
	entries, err := fsreq.File.ReadDir(-1)
	if err != nil {
		httputil.HTTPError(rw, r, "Error Reading Dir", http.StatusInternalServerError, err)
		return
	}

	// First index entry is the 'dotdot'
	files := make([]file, 1, 1+len(entries))
	files[0] = file{
		Name:    "../",
		Path:    dotdot(r.URL.Path),
		Size:    bytesize.Size(fsreq.Stat.Size()),
		Mode:    fsreq.Stat.Mode(),
		ModTime: fsreq.Stat.ModTime(),
	}

	// Convert entries to tplate file objs
	for _, entry := range entries {
		name := entry.Name()

		// Build filesystem path
		fsreq.PBuf.Reset()
		fsreq.PBuf.Append(fsreq.RawPath)
		fsreq.PBuf.Append(name)

		// Check for restricted filesystem path
		if !fs.restricted(fsreq.PBuf.String()) {
			// Get file entry info
			stat, err := entry.Info()
			if err != nil {
				continue
			}

			// Build URL path
			fsreq.PBuf.Reset()
			fsreq.PBuf.Append(r.URL.Path)
			fsreq.PBuf.Append(name)

			if entry.IsDir() {
				// Dir paths need trailing '/'
				fsreq.PBuf.B = append(fsreq.PBuf.B, '/')
				name += "/"
			}

			// Append the new file obj
			files = append(files, file{
				Name:    name,
				Path:    string(fsreq.PBuf.B), // copy
				Size:    bytesize.Size(stat.Size()),
				Mode:    stat.Mode(),
				ModTime: stat.ModTime(),
			})
		}
	}

	// Finally, sort files
	fs.SortFiles(files[1:])

	// Generate tplate index struct
	index := struct {
		DirPath string
		Files   []file
	}{
		DirPath: r.URL.Path,
		Files:   files,
	}

	// Execute the template into a byte buffer
	if err := fs.IndexTemplate.Execute(&fsreq.Buf, index); err != nil {
		httputil.HTTPError(rw, r, "Error Rendering Template", http.StatusInternalServerError, err)
		return
	}

	// Set content-type and write template buffer to client
	rw.Header().Set("Content-Type", "text/html; charset=utf-8")
	if _, err := rw.Write(fsreq.Buf.B); err != nil {
		log.ErrorKVs(kv.Fields{
			{K: "id", V: getRequestID(r)},
			{K: "error", V: err},
			{K: "msg", V: "ResponseWriter.Write() error"},
		}...)
		return
	}
}

// restricted returns if provided path is restricted.
func (fs *FS) restricted(path string) bool {
	for _, expr := range fs.Restricted {
		if expr.Match(path) {
			return true
		}
	}
	return false
}

// file represents a file object passed into
// the template renderer for generating indices.
type file struct {
	ModTime time.Time
	Name    string
	Path    string
	Size    bytesize.Size
	Mode    fs.FileMode
}
