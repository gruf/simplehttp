package main

import (
	"bytes"
	"fmt"
	"strings"

	"codeberg.org/gruf/go-byteutil"
	"codeberg.org/gruf/go-kv"
	"codeberg.org/gruf/go-split"
	"golang.org/x/crypto/bcrypt"
)

// multiUserAuth implements a middleware.Authenticator
// solution for multiple usernames with corresponding
// bcrypt hashed passwords, to provide HTTP basic auth.
type multiUserAuth struct{ users map[string][]byte }

// Authenticate: implements middleware.Authenticator{}.Authenticate.
func (auth *multiUserAuth) Authenticate(username, password string) bool {
	// Find stored hash for username.
	hash, ok := auth.users[username]
	if !ok {
		return false
	}

	// Compare pre-hashed password and input.
	if err := bcrypt.CompareHashAndPassword(
		hash,
		byteutil.S2B(password),
	); err != nil {

		if err != bcrypt.ErrMismatchedHashAndPassword {
			panic(err) // likely bad provided hash.
		}

		// Incorrect.
		return false
	}

	return true
}

// userPassMap represents a configuration value
// map of usernames to hashed password values,
// with required methods for flag value parsing.
type userPassMap struct{ v map[string][]byte }

// Set: implements fflag.Value{}.Set.
func (v *userPassMap) Set(in string) error {

	// Split input in case multiple were given.
	combos, err := split.SplitStrings[string](in)
	if err != nil {
		return err
	}

	if len(combos) > 0 {
		if v.v == nil {
			// Ensure map is allocated.
			v.v = make(map[string][]byte)
		}

		for _, combo := range combos {
			// Split combo into username and pw hash.
			user, hash := splitUserPasswordHash(combo)

			// Check for an unsafe(password) format hash, which
			// indicates they've given us a password to hash.
			hash, err := convertUnsafePassword(hash)
			if err != nil {
				return err
			}

			// Add to map.
			v.v[user] = hash
		}
	}

	return nil
}

// Kind: implements fflag.Value{}.Kind.
func (v *userPassMap) Kind() string {
	return `[]username:bcrypt_hash`
}

// String: implements fmt.Stringer{}.String.
func (v userPassMap) String() string {
	// Buffer size
	// to prealloc.
	total := 0

	// Convert map to slice of usernames.
	users := make([]string, 0, len(v.v))
	for user := range v.v {
		total += len(users) + 1
		users = append(users, user)
	}

	if len(users) == 0 {
		return ""
	}

	// Prepare buffer of size.
	var buf byteutil.Buffer
	buf.Guarantee(total)

	// Append quoted users to buf.
	for _, user := range users {
		kv.AppendQuoteString(&buf, user)
		buf.B = append(buf.B, `,`...)
	}

	// Return without
	// trailing comma.
	buf.Truncate(1)
	return buf.String()
}

// splitUserPasswordHash ...
func splitUserPasswordHash(in string) (username string, hash []byte) {
	i := strings.Index(in, ":")
	if i < 0 {
		username = in
	} else {
		username = in[:i]
		hash = []byte(in[i+1:])
	}
	return
}

// convertUnsafePassword ...
func convertUnsafePassword(hash []byte) ([]byte, error) {
	password, unsafe := isUnsafePassword(hash)
	if !unsafe {
		return hash, nil
	}

	hash, err := bcrypt.GenerateFromPassword(
		password,
		bcrypt.DefaultCost,
	)
	if err != nil {
		return nil, fmt.Errorf("%s: %v", password, err)
	}

	return hash, nil
}

// isUnsafePassword ...
func isUnsafePassword(in []byte) ([]byte, bool) {
	var (
		prefix = []byte("unsafe(")
		suffix = []byte(")")
	)
	if len(in) == 0 {
		// we actually treat an empty
		// string as just no password.
		return nil, true
	}
	if !bytes.HasPrefix(in, prefix) {
		return nil, false
	}
	if !bytes.HasSuffix(in, suffix) {
		return nil, false
	}
	return in[len(prefix) : len(in)-len(suffix)], true
}
