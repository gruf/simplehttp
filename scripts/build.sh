#!/bin/sh

set -e

# Print message and exit
die() { echo "$*"; exit 1; }

# Log and execute provided args
log_exec() { echo "$ ${*}"; "${@}"; }

# Check whether debug enabled
is_debug() { [ ! -z "$DEBUG" ]; }

# POSIX compatible readlink -F
posix_readlink() { (cd "$1"; pwd -P); }

# Ensure required environment variables
[ -z "$BUILD_DIR" ] && die '$BUILD_DIR not set'
[ -z "$BUILD_PKG" ] && die '$BUILD_PKG not set'
[ -z "$BUILD_OUT" ] && die '$BUILD_OUT not set'

# Enter build dir (using abs path)
cd "$(posix_readlink "${BUILD_DIR}")"

# Determine git version information
COMMIT=$(git rev-parse --short HEAD)
VERSION=$(git name-rev --name-only ${COMMIT})
VERSION=${VERSION#tags/}

# Debug environment info
is_debug && cat << EOF
COMMIT=${COMMIT}
VERSION=${VERSION}
BUILD_DIR=${BUILD_DIR}
BUILD_PKG=${BUILD_PKG}
BUILD_OUT=${BUILD_OUT}
GOARCH=${GOARCH}
GOOS=${GOOS}
EOF

# Start the build!
log_exec env \
CGO_ENABLED=0 \
GOOS=$GOOS GOARCH=$GOARCH GOAMD64=$GOAMD64 \
go build -trimpath $(is_debug && echo '-v') \
         -tags "kvformat netgo osusergo static_build $(is_debug && echo debug)" \
         -ldflags=all="-s -w" \
         -gcflags=all='-l=4' \
         -o $BUILD_OUT \
         "$BUILD_PKG"