package main

import (
	"net/http"
	"slices"
	"strings"

	"codeberg.org/gruf/go-middleware"
)

// sortByName sorts files by name.
func sortByName(files []file) {
	const k = -1
	slices.SortFunc(files, func(a, b file) int {
		if a.Name < b.Name {
			return +k
		} else if a.Name > b.Name {
			return -k
		}
		return 0
	})
}

// sortBySize sorts files by size.
func sortBySize(files []file) {
	const k = +1
	slices.SortFunc(files, func(a, b file) int {
		if a.Size < b.Size {
			return +k
		} else if a.Size > b.Size {
			return -k
		}
		return 0
	})
}

// sortByDate sorts files by date.
func sortByDate(files []file) {
	const k = +1
	slices.SortFunc(files, func(a, b file) int {
		if a.ModTime.Before(b.ModTime) {
			return +k
		} else if a.ModTime.After(b.ModTime) {
			return -k
		}
		return 0
	})
}

// getRequestID returns the request ID for given HTTP request.
func getRequestID(r *http.Request) string {
	id, _ := middleware.GetRequestID(r)
	return id
}

// dotdot will return rooted URI
// path with last element removed.
func dotdot(path string) string {
	if len(path) == 1 {
		return "/"
	} // ^ path is always rooted
	path = path[:len(path)-1]
	idx := strings.LastIndexByte(path, '/')
	if idx == 0 {
		return "/"
	} // ^ this was last path element
	return path[:idx]
}
