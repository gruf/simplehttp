module codeberg.org/gruf/simplehttp

go 1.22

require (
	codeberg.org/gruf/go-bytesize v1.0.3
	codeberg.org/gruf/go-byteutil v1.3.0
	codeberg.org/gruf/go-debug v1.3.0
	codeberg.org/gruf/go-fastpath/v2 v2.0.0
	codeberg.org/gruf/go-fflag v0.0.0-20241007195749-357506927a2c
	codeberg.org/gruf/go-glob v0.1.1
	codeberg.org/gruf/go-kv v1.6.5
	codeberg.org/gruf/go-logger/v4 v4.0.5
	codeberg.org/gruf/go-middleware v1.2.1
	codeberg.org/gruf/go-mimetypes v1.2.0
	codeberg.org/gruf/go-split v1.2.0
	golang.org/x/crypto v0.16.0
)

require (
	codeberg.org/gruf/go-ctx v1.0.2 // indirect
	codeberg.org/gruf/go-ulid v1.1.0 // indirect
	github.com/felixge/httpsnoop v1.0.4 // indirect
)
