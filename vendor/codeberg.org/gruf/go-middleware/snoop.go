package middleware

import (
	"net/http"

	"github.com/felixge/httpsnoop"
)

// snoopStatusCode snoops on an HTTP request and response cycle's status code.
func snoopStatusCode(rw http.ResponseWriter) (http.ResponseWriter, *int) {
	var status int
	return httpsnoop.Wrap(rw, httpsnoop.Hooks{
		WriteHeader: func(set httpsnoop.WriteHeaderFunc) httpsnoop.WriteHeaderFunc {
			return func(code int) {
				if status == 0 {
					status = code
				}
				set(code)
			}
		},
	}), &status
}
