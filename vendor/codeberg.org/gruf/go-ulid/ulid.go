package ulid

import (
	"bytes"
	"crypto/rand"
	"database/sql/driver"
	"encoding/base32"
	"errors"
	"fmt"
	"unsafe"
)

const (
	// CrockfordBase32 is the ULID encoding alphabet, missing I,L,O,U
	// to avoid confusion and abuse. See: http://www.crockford.com/wrmg/base32.html
	CrockfordBase32 = "0123456789ABCDEFGHJKMNPQRSTVWXYZ"

	// EncodedLen is the length of a text encoded ULID.
	EncodedLen = (BinaryLen*8 + 4) / 5

	// BinaryLen is the length of a binary ULID.
	BinaryLen = len(ULID{})
)

var (
	// ErrInvalidLength is returned when a parse/unmarshal is attempted on data of incorrect length.
	ErrInvalidLength = errors.New("ulid: invalid data length")

	// ErrInvalidULID is returned when an invalid text encoding of of a ULID is attempted to be parsed
	// or unmarshaled. Either due to illegal crockford base32 input data, or due to final character being
	// outside the possible decodable range of a binary ULID*.
	//
	// * due to the 130bit base32 -> 128bit binary decoding losing the
	// final 2 bits of data, only the following are possible final chars:
	//
	// 0   4   8   12  16  20  24  28
	// 0123456789ABCDEFGHJKMNPQRSTVWXYZ
	// 0,  4,  8,  C,  G,  M,  R,  W
	//
	// 0:  00000
	// 4:  00100
	// 8:  01000
	// 12: 01100
	// 16: 10000
	// 20: 10100
	// 24: 11000
	// 28: 11100
	//
	// All credit to https://codeberg.org/fansipans for catching and suggesting this fix <3.
	ErrInvalidULID = errors.New("ulid: invalid ulid data")

	// ErrBadScanSource is returned when an SQL scan is attempted with a non string/[]byte source.
	ErrBadScanSource = errors.New("ulid: scan src must be string or []byte")

	// base32enc is a pre-initialized CrockfordBase32 encoding without any padding.
	base32enc = base32.NewEncoding(CrockfordBase32).WithPadding(base32.NoPadding)
)

// Encoding returns the ULID base32 encoding prepared using CrockfordBase32 and no padding.
func Encoding() base32.Encoding {
	return *base32enc
}

// ULID: Universally Unique Lexicographically Sortable Identifier.
type ULID [16]byte

// Max returns the highest possible value ULID.
func Max() ULID {
	return ULID{
		0xff, 0xff, 0xff, 0xff,
		0xff, 0xff, 0xff, 0xff,
		0xff, 0xff, 0xff, 0xff,
		0xff, 0xff, 0xff, 0xff,
	}
}

// New calculates a new ULID for current time using global MonotonicReader.
func New() (ulid ULID, err error) {
	mutex.Lock()
	if reader == nil {
		// set default safe source
		err = setreader(rand.Reader)
	}
	if err == nil {
		// load next ULID from reader
		err = reader.Next(Now(), &ulid)
	}
	mutex.Unlock()
	return
}

// MustNew calls .New() and panics if the error is non-nil.
func MustNew() ULID {
	ulid, err := New()
	if err != nil {
		panic(err)
	}
	return ulid
}

// Parse will parse a text-encoded ULID from bytes.
func Parse(b []byte) (ULID, error) {
	var ulid ULID
	err := ulid.UnmarshalText(b)
	return ulid, err
}

// ParseString will parse a text-encoded ULID from string.
func ParseString(s string) (ULID, error) {
	var ulid ULID
	err := ulid.UnmarshalText(s2b(s))
	return ulid, err
}

// Timestamp returns the timestamp encoded in this ULID.
func (u ULID) Timestamp() TS {
	ts := uint64(u[5]) | uint64(u[4])<<8 |
		uint64(u[3])<<16 | uint64(u[2])<<24 |
		uint64(u[1])<<32 | uint64(u[0])<<40
	return TS(ts)
}

// AppendFormat append text encoded ULID to 'dst'.
func (u ULID) AppendFormat(dst []byte) []byte {
	length := len(dst)

	if cap(dst)-length < EncodedLen {
		// Allocate larger slice for ULID
		capac := 2*cap(dst) + EncodedLen
		dst2 := make([]byte, length, capac)
		copy(dst2, dst)
		dst = dst2
	}

	// Reslice to support encoded len
	dst = dst[:length+EncodedLen]

	// Perform actual encoding
	into := dst[length : length+EncodedLen]
	base32enc.Encode(into, u[:])

	return dst
}

// Bytes returns text encoded bytes of receiving ULID.
func (u ULID) Bytes() []byte {
	dst := make([]byte, 0, EncodedLen)
	return u.AppendFormat(dst)
}

// String returns text encoded bytes of receiving ULID.
func (u ULID) String() string {
	b := u.Bytes()
	p := unsafe.Pointer(&b)
	return *(*string)(p)
}

// MarshalBinary implements encoding.BinaryMarshaler.
func (u ULID) MarshalBinary() ([]byte, error) {
	return u[:], nil
}

// MarshalText implements encoding.TextMarshaler.
func (u ULID) MarshalText() ([]byte, error) {
	return u.Bytes(), nil
}

// UnmarshalBinary implements encoding.BinaryUnmarshaler.
func (u *ULID) UnmarshalBinary(b []byte) error {
	if len(b) != BinaryLen {
		return ErrInvalidLength
	}
	copy(u[:], b)
	return nil
}

// UnmarshalText implements encoding.TextUnmarshaler.
func (u *ULID) UnmarshalText(b []byte) error {
	if len(b) != EncodedLen {
		return ErrInvalidLength
	}

	switch c := b[EncodedLen-1]; c {
	// Check for valid decodable final ULID text char.
	case '0', '4', '8', 'C', 'G', 'M', 'R', 'W':
	default:
		return fmt.Errorf("%w: '%c' outside encoding range", ErrInvalidULID, c)
	}

	// Base32 decode into receiving ULID.
	_, err := base32enc.Decode(u[:], b)
	if err != nil {
		return fmt.Errorf("%w: %w", ErrInvalidULID, err)
	}

	return nil
}

// Scan implements sql.Scanner.
func (u *ULID) Scan(src interface{}) error {
	switch src := src.(type) {
	case nil:
		return nil
	case string:
		return u.UnmarshalText(s2b(src))
	case []byte:
		return u.UnmarshalBinary(src)
	default:
		return ErrBadScanSource
	}
}

// Value implements driver.Valuer.
func (u ULID) Value() (driver.Value, error) {
	return u.MarshalBinary()
}

// Compare returns integer value from comparison between
// receiving and argument ULID lexographically.
//
// 0 -> (u == o), -1 -> (u < o), 1 -> (u > 0).
func (u ULID) Compare(o ULID) int {
	return bytes.Compare(u[:], o[:])
}
