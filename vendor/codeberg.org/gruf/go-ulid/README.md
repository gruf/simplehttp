# ULID

Universally Unique Lexicographically Sortable Identifier.

A GUID/UUID can be suboptimal for many use-cases because:

- it isn't the most character efficient way of encoding 128 bits

- provides no information other than randomness (depending on revision)

A ULID however:

- compatible with UUID/GUID's

- 1.21e+24 unique ULIDs per millisecond (1,208,925,819,614,629,174,706,176 to be exact)

- lexicographically sortable

- canonically encoded as a 26 character string

- uses Crockford's base32 for better efficiency and readability (5 bits per character)

- case insensitive

- no special characters (URL safe)

- monotonic sort order

## Specification

Below is the current specification of ULID as implemented in this repository:

```
 01AN4Z07BY      79KA1307SR9X4MV3
|----------|    |----------------|
 Timestamp           Entropy
  10 chars           16 chars
   48bits             80bits
   base32             base32
```

## Components

Timestamp:

- 48 bit integer

- UNIX-time in milliseconds

- won't run out of space till the year 10895 AD (specified by `ulid.MaxTime()`)

Entropy:

- 80 random (dependent on `io.Reader` implementation you provide) bits

- Monotonicity within same millisecond

## Encoding

Crockford's Base32 is used as shown. This alphabet excludes the letters I, L, O, and U to avoid confusion and abuse.

```
0123456789ABCDEFGHJKMNPQRSTVWXYZ
```

## Binary Layout and Byte Order

The components are encoded as 16 octets. Each component is encoded with the Most Significant Byte first (network byte order).

```
  0                   1                   2                   3
 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|                      32_bit_uint_time_high                    |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|     16_bit_uint_time_low      |       16_bit_uint_random      |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|                       32_bit_uint_random                      |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|                       32_bit_uint_random                      |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
```

## Tests

This module's tests can be run with `ITERATIONS=100 go test ./.` where `$ITERATIONS` specifies the number of test iterations to perform, i.e. how many ULIDs to generate and validate. By default this value is `10,000,000`.

## Thanks

This module initially started as work to fix some issues in [oklog's ULID package](https://github.com/oklog/ulid), but soon gathered enough changes that I rewrote it from scratch.