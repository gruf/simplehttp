package ulid

import "time"

// maximum possible encodable timestamp.
const maxTS = TS(uint64(0xff) | uint64(0xff)<<8 |
	uint64(0xff)<<16 | uint64(0xff)<<24 |
	uint64(0xff)<<32 | uint64(0xff)<<40)

// MaxTime is the largest possbile timestamp encodable within a ULID.
func MaxTime() time.Time {
	return maxTS.Time()
}

// TS represents a Unix time in milliseconds.
type TS uint64

// Now returns the current time as timestamp.
func Now() TS { return Timestamp(time.Now()) }

// Timestamp will return time t as a timestamp.
func Timestamp(t time.Time) TS {
	s := uint64(t.Unix()) * 1e3
	ns := uint64(t.Nanosecond()) / 1e6
	return TS(s + ns)
}

// Add returns the timestamp + d.
func (ts TS) Add(d time.Duration) TS {
	return ts + TS(d.Milliseconds())
}

// Sub returns the timestamp - d.
func (ts TS) Sub(d time.Duration) TS {
	return ts - TS(d.Milliseconds())
}

// After returns whether the timestamp is after t.
func (ts TS) After(t time.Time) bool {
	return ts > Timestamp(t)
}

// Before returns whether the timestamp is before t.
func (ts TS) Before(t time.Time) bool {
	return ts < Timestamp(t)
}

// Equal returns whether the timestamp is equal to t.
func (ts TS) Equal(t time.Time) bool {
	return ts == Timestamp(t)
}

// Time returns this timestamp as a time.Time.
func (ts TS) Time() time.Time {
	s := int64(ts / 1e3)
	ns := int64(ts%1e3) * 1e6
	return time.Unix(s, ns)
}
