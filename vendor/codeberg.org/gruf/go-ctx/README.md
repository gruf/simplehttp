# go-ctx

A reusable context meant to be used more a single instance that is passed down a stack of functions.