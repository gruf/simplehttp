package logger

import (
	"fmt"
	"io"
	"os"
	"runtime"
	"sync/atomic"
	"time"

	"codeberg.org/gruf/go-byteutil"
	"codeberg.org/gruf/go-kv"
)

const (
	// Logger entry formatting flags.
	LTimestamp = uint32(1) << 1
	LCaller    = uint32(1) << 2
)

type Logger struct {
	pack uint64        // packed uint8(LEVEL)|uint32(FLAGS)
	now  func() string // returns current timestamp
	out  io.Writer     // log outut writer
}

func New(opts ...Option) *Logger {
	l := new(Logger)

	// Set starting defaults.
	l.store(ALL, LTimestamp)

	// Apply given options.
	for _, opt := range opts {
		opt.Apply(l)
	}

	if l.now == nil {
		// set default timestamping
		l.now = func() string {
			const format = `"02/01/2006 15:04:05"`
			return time.Now().Format(format)
		}
	}

	if l.out == nil {
		// set default output writer
		l.out = &safeWriter{Writer: os.Stderr}
	}

	return l
}

func (l *Logger) Entry() Entry {
	return Entry{
		Lvl:  UNSET,
		Data: nil,
		Msg:  "",
		Out:  l,
	}
}

func (l *Logger) Trace(args ...interface{}) {
	l.Write(2, TRACE, func(buf *byteutil.Buffer) {
		buf.B = append(buf.B, `msg=`...)
		kv.AppendQuoteString(buf, fmt.Sprint(args...))
	})
}

func (l *Logger) Tracef(format string, args ...interface{}) {
	l.Write(2, TRACE, func(buf *byteutil.Buffer) {
		buf.B = append(buf.B, `msg=`...)
		kv.AppendQuoteString(buf, fmt.Sprintf(format, args...))
	})
}

func (l *Logger) TraceKV(key string, value any) {
	l.Write(2, TRACE, func(buf *byteutil.Buffer) {
		kv.Field{K: key, V: value}.AppendFormat(buf, false)
	})
}

func (l *Logger) TraceKVs(fields ...kv.Field) {
	l.Write(2, TRACE, func(buf *byteutil.Buffer) {
		kv.Fields(fields).AppendFormat(buf, false)
	})
}

func (l *Logger) Debug(args ...interface{}) {
	l.Write(2, DEBUG, func(buf *byteutil.Buffer) {
		buf.B = append(buf.B, `msg=`...)
		kv.AppendQuoteString(buf, fmt.Sprint(args...))
	})
}

func (l *Logger) Debugf(format string, args ...interface{}) {
	l.Write(2, DEBUG, func(buf *byteutil.Buffer) {
		buf.B = append(buf.B, `msg=`...)
		kv.AppendQuoteString(buf, fmt.Sprintf(format, args...))
	})
}

func (l *Logger) DebugKV(key string, value any) {
	l.Write(2, DEBUG, func(buf *byteutil.Buffer) {
		kv.Field{K: key, V: value}.AppendFormat(buf, false)
	})
}

func (l *Logger) DebugKVs(fields ...kv.Field) {
	l.Write(2, DEBUG, func(buf *byteutil.Buffer) {
		kv.Fields(fields).AppendFormat(buf, false)
	})
}

func (l *Logger) Info(args ...interface{}) {
	l.Write(2, INFO, func(buf *byteutil.Buffer) {
		buf.B = append(buf.B, `msg=`...)
		kv.AppendQuoteString(buf, fmt.Sprint(args...))
	})
}

func (l *Logger) Infof(format string, args ...interface{}) {
	l.Write(2, INFO, func(buf *byteutil.Buffer) {
		buf.B = append(buf.B, `msg=`...)
		kv.AppendQuoteString(buf, fmt.Sprintf(format, args...))
	})
}

func (l *Logger) InfoKV(key string, value any) {
	l.Write(2, INFO, func(buf *byteutil.Buffer) {
		kv.Field{K: key, V: value}.AppendFormat(buf, false)
	})
}

func (l *Logger) InfoKVs(fields ...kv.Field) {
	l.Write(2, INFO, func(buf *byteutil.Buffer) {
		kv.Fields(fields).AppendFormat(buf, false)
	})
}

func (l *Logger) Warn(args ...interface{}) {
	l.Write(2, WARN, func(buf *byteutil.Buffer) {
		buf.B = append(buf.B, `msg=`...)
		kv.AppendQuoteString(buf, fmt.Sprint(args...))
	})
}

func (l *Logger) Warnf(format string, args ...interface{}) {
	l.Write(2, WARN, func(buf *byteutil.Buffer) {
		buf.B = append(buf.B, `msg=`...)
		kv.AppendQuoteString(buf, fmt.Sprintf(format, args...))
	})
}

func (l *Logger) WarnKV(key string, value any) {
	l.Write(2, WARN, func(buf *byteutil.Buffer) {
		kv.Field{K: key, V: value}.AppendFormat(buf, false)
	})
}

func (l *Logger) WarnKVs(fields ...kv.Field) {
	l.Write(2, WARN, func(buf *byteutil.Buffer) {
		kv.Fields(fields).AppendFormat(buf, false)
	})
}

func (l *Logger) Error(args ...interface{}) {
	l.Write(2, ERROR, func(buf *byteutil.Buffer) {
		buf.B = append(buf.B, `msg=`...)
		kv.AppendQuoteString(buf, fmt.Sprint(args...))
	})
}

func (l *Logger) Errorf(format string, args ...interface{}) {
	l.Write(2, ERROR, func(buf *byteutil.Buffer) {
		buf.B = append(buf.B, `msg=`...)
		kv.AppendQuoteString(buf, fmt.Sprintf(format, args...))
	})
}

func (l *Logger) ErrorKV(key string, value any) {
	l.Write(2, ERROR, func(buf *byteutil.Buffer) {
		kv.Field{K: key, V: value}.AppendFormat(buf, false)
	})
}

func (l *Logger) ErrorKVs(fields ...kv.Field) {
	l.Write(2, ERROR, func(buf *byteutil.Buffer) {
		kv.Fields(fields).AppendFormat(buf, false)
	})
}

func (l *Logger) Panic(args ...interface{}) {
	str := fmt.Sprint(args...)
	l.Write(2, PANIC, func(buf *byteutil.Buffer) {
		buf.B = append(buf.B, `msg=`...)
		kv.AppendQuoteString(buf, str)
	})
	panic(str)
}

func (l *Logger) Panicf(format string, args ...interface{}) {
	str := fmt.Sprintf(format, args...)
	l.Write(2, PANIC, func(buf *byteutil.Buffer) {
		buf.B = append(buf.B, `msg=`...)
		kv.AppendQuoteString(buf, str)
	})
	panic(str)
}

func (l *Logger) PanicKV(key string, value any) {
	str := kv.Field{K: key, V: value}.String()
	l.Write(2, PANIC, func(buf *byteutil.Buffer) {
		buf.B = append(buf.B, str...)
	})
	panic(str)
}

func (l *Logger) PanicKVs(fields ...kv.Field) {
	str := kv.Fields(fields).String()
	l.Write(2, PANIC, func(buf *byteutil.Buffer) {
		buf.B = append(buf.B, str...)
	})
	panic(str)
}

func (l *Logger) Print(args ...interface{}) {
	l.Write(2, UNSET, func(buf *byteutil.Buffer) {
		fmt.Fprint(buf, args...)
	})
}

func (l *Logger) Printf(format string, args ...interface{}) {
	l.Write(2, UNSET, func(buf *byteutil.Buffer) {
		fmt.Fprintf(buf, format, args...)
	})
}

func (l *Logger) PrintKV(key string, value any) {
	l.Write(2, UNSET, func(buf *byteutil.Buffer) {
		kv.Field{K: key, V: value}.AppendFormat(buf, false)
	})
}

func (l *Logger) PrintKVs(fields ...kv.Field) {
	l.Write(2, UNSET, func(buf *byteutil.Buffer) {
		kv.Fields(fields).AppendFormat(buf, false)
	})
}

// Output will write message string at level with calldepth.
//
// Note this function cannot be inlined, to ensure expected
// and consistent behaviour in setting trace / caller info.
//
//go:noinline
func (l *Logger) Output(calldepth int, lvl LEVEL, msg string) {
	l.Write(calldepth+1, lvl, func(buf *byteutil.Buffer) {
		buf.B = append(buf.B, msg...)
	})
}

// Write will output contents of buffer after write function,
// at level using supplied calldepth for caller calculation.
//
// Note this function cannot be inlined, to ensure expected
// and consistent behaviour in setting trace / caller info.
//
//go:noinline
func (l *Logger) Write(calldepth int, lvl LEVEL, write func(*byteutil.Buffer)) {
	// Load current lvl + flags
	clvl, flags := l.load()
	if !clvl.CanLog(lvl) {
		return
	}

	// Acquire buffer
	buf := getBuf()

	if flags&LTimestamp != 0 {
		// Append formatted timestamp
		buf.B = append(buf.B, `time=`...)
		buf.B = append(buf.B, l.now()...)
		buf.B = append(buf.B, ' ')
	}

	if flags&LCaller != 0 {
		// Get caller information.
		pcs := make([]uintptr, 1)
		_ = runtime.Callers(calldepth+1, pcs)
		fn := runtime.FuncForPC(pcs[0])

		// Append formatted caller func.
		buf.B = append(buf.B, `func=`...)
		buf.B = append(buf.B, funcName(fn)...)
		buf.B = append(buf.B, ' ')
	}

	if lvl := levels[lvl]; lvl != "" {
		// Append formatted level string
		buf.B = append(buf.B, `level=`...)
		buf.B = append(buf.B, lvl...)
		buf.B = append(buf.B, ' ')
	}

	// Write message
	write(buf)

	if buf.B[len(buf.B)-1] != '\n' {
		// Append a final newline
		buf.B = append(buf.B, '\n')
	}

	// Write to log and release
	_, _ = l.out.Write(buf.B)
	putBuf(buf)
}

// Level returns the minimum logging level for the logger.
func (l *Logger) Level() LEVEL {
	lvl, _ := l.load()
	return lvl
}

// SetLevel sets the minimum logging level for the logger.
func (l *Logger) SetLevel(lvl LEVEL) {
	l.store(lvl, l.Flags())
}

// Flags returns the currently set logger flags.
func (l *Logger) Flags() uint32 {
	_, flags := l.load()
	return flags
}

// SetFlags sets the flags for the logger.
func (l *Logger) SetFlags(flags uint32) {
	l.store(l.Level(), flags)
}

// Writer returns the output destination for the logger.
func (l *Logger) Writer() io.Writer {
	return l.out
}

// SetOutput sets the output destination for the logger.
func (l *Logger) SetOutput(w io.Writer) {
	l.out = w
}

// store will pack given LEVEL and logger flags, storing atomically in the 'pack' member.
func (l *Logger) store(lvl LEVEL, flags uint32) {
	const bits = 32
	const mask = (1 << bits) - 1
	u1, u2 := uint64(lvl), uint64(flags)
	atomic.StoreUint64(&l.pack, u1<<bits|u2&mask)
}

// load will atomically load the 'pack' member, and unpack the current LEVEL and logger flags.
func (l *Logger) load() (lvl LEVEL, flags uint32) {
	const bits = 32
	const mask = (1 << bits) - 1
	u := atomic.LoadUint64(&l.pack)
	u1, u2 := uint32(u>>bits), uint32(u&mask)
	return LEVEL(u1), u2
}
