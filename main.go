package main

import (
	"maps"
	"net/http"
	"os"
	"os/signal"
	"path"
	"strings"
	"syscall"
	"time"

	"codeberg.org/gruf/go-bytesize"
	"codeberg.org/gruf/go-debug"
	"codeberg.org/gruf/go-fflag"
	"codeberg.org/gruf/go-glob"
	"codeberg.org/gruf/go-kv"
	"codeberg.org/gruf/go-logger/v4"
	"codeberg.org/gruf/go-logger/v4/httplog"
	"codeberg.org/gruf/go-middleware"
)

var (
	// log is the global
	// logger instance.
	log = logger.New(
		logger.WithOutput(os.Stdout),
		logger.WithTimestamp(true),
		logger.WithCaller(false),
		logger.WithLevel(logger.ALL),
		logger.WithClock(func() string {
			const format = `"2006-01-02 15:04:05.000"`
			return time.Now().Format(format)
		}),
	)

	// httputil is the global HTTP middleware instance.
	httputil = middleware.New((*httplog.Logger)(log))
)

// config defines server CLI flag info
// with unmarshaling destinations.
var config = struct {
	BasicAuth   userPassMap   `short:"u" long:"auth-user" usage:"Set permitted HTTP basic authentication users"`
	Listen      string        `short:"l" long:"listen" usage:"Server listen address" default:"127.0.0.1:8080"`
	TLSCert     string        `short:"c" long:"tls-cert" usage:"Server TLS cert file" default:""`
	TLSKey      string        `short:"k" long:"tls-key" usage:"Server TLS key file" default:""`
	RootDir     string        `short:"d" long:"root" usage:"Root filesystem directory to serve" default:""`
	IndexTplate string        `short:"t" long:"index-template" usage:"Go HTML template path used to generate directory indices" default:""`
	SortBy      string        `short:"s" long:"sort-by" usage:"On index page generation, sort files by attribute (name,date,size)" default:"name"`
	Restricted  []string      `short:"r" long:"restricted" usage:"Restricted filesystem path glob expressions" default:"*/.*"`
	IndexFiles  []string      `short:"i" long:"index-files" usage:"Index files used in directory paths" default:""`
	BufferSize  bytesize.Size `short:"b" long:"buffer-size" usage:"File read and template execution buffer size" default:"4096B"`
	AcceptRange bool          `short:"a" long:"accept-ranges" usage:"Accept and handle range headers" default:"true"`
	GenIndex    bool          `short:"g" long:"generate-index" usage:"Generate index page when no index files found" default:"false"`
}{}

func main() {
	var err error

	// Register CLI flags.
	fflag.AutoEnv(nil)
	fflag.StructFields(&config)
	fflag.Version(debug.BuildInfo("simplehttp"))
	fflag.Help()

	// Parse CLI flags into config.
	if extra := fflag.MustParse(); len(extra) > 0 {
		os.Stdout.WriteString("" +
			"Usage: " + os.Args[0] + " ...\n" +
			fflag.Usage() + "\n" +
			"unexpected cli args: " + strings.Join(extra, " ") + "\n",
		)
		syscall.Exit(1)
	}

	fs := FS{
		// Initialize FS using configuration
		AcceptRange:   config.AcceptRange,
		GenerateIndex: config.GenIndex,
		IndexFiles:    config.IndexFiles,
		RootDir:       path.Clean(config.RootDir),
	}

	// Compile restricted path exprs
	for _, expr := range config.Restricted {
		fs.Restricted = append(
			fs.Restricted,
			glob.Compile(expr),
		)
	}

	// Check for index tplate (falling back to default)
	fs.IndexTemplate, err = ParseTemplate(config.IndexTplate)
	if err != nil {
		panic(err)
	}

	// Set file sort function
	switch config.SortBy {
	case "date":
		fs.SortFiles = sortByDate
	case "name":
		fs.SortFiles = sortByName
	case "size":
		fs.SortFiles = sortBySize
	default:
		panic("unknown sort type: " + config.SortBy)
	}

	if config.BufferSize < 512 {
		// Bufsize must be >= 512 bytes
		// (content mime buffer length)
		config.BufferSize = 4096
	}

	// Prepare request pool allocator
	fs.pool.New = func() interface{} {
		return NewFSRequest(int(config.BufferSize))
	}

	// Wrap handlers with log + recovery
	// (pprof will be added when debug enabled)
	handler := debug.WithPprof(&fs)
	handler = httputil.Recovery(handler)
	handler = httputil.Logger(handler)
	handler = httputil.RequestID(handler)

	if len(config.BasicAuth.v) > 0 {
		// Add basic auth handler if auth users provided.
		auth := multiUserAuth{maps.Clone(config.BasicAuth.v)} // clone to drop extra space
		handler = httputil.BasicAuth(handler, &auth)
	}

	// Setup the HTTP server.
	srv := http.Server{
		Addr:    config.Listen,
		Handler: handler,
	}

	// Log server configuration
	log.InfoKVs(kv.Field{
		K: "config",
		V: config,
	})

	// Listen for OS signals
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)

	go func() {
		log.Infof(`received signal %v, shutting down...`, <-sigs)
		_ = srv.Close() // shutdown server
	}()

	switch {
	// TLS cert and key file provided, use HTTPS
	case config.TLSCert != "" && config.TLSKey != "":
		log.Infof("listening on https://%s", config.Listen)
		err = srv.ListenAndServeTLS(config.TLSCert, config.TLSKey)

	// Use regular HTTP
	default:
		log.Infof("listening on: http://%s", config.Listen)
		err = srv.ListenAndServe()
	}

	// Log all errors (except server closed)
	if err != nil && err != http.ErrServerClosed {
		panic("error listening on HTTP server: " + err.Error())
	}
}
