FROM docker.io/golang:1.21-alpine as gobuild

# Add git to build-root
RUN set -x; \
    apk update && \
    apk add git

# Enter workdir
WORKDIR '/repo'
COPY . .

# Set build env
ENV BUILD_DIR='.'
ENV BUILD_PKG='.'
ENV BUILD_OUT='service'

# Build package!
RUN './scripts/build.sh'

FROM docker.io/alpine:latest

# Add ca-certs + timezones
# to the root container
RUN set -x; apk update && \
    apk add ca-certificates tzdata

# Add service user
RUN set -x ;\
    addgroup 'service' && \
    adduser -D -h '/app' -G 'service' 'service' && \
    mkdir -p '/app/data' && \
    chown service:service '/app/data'

# Copy in the build output
COPY --from=gobuild \
    --chown=service:service \
    '/repo/service' \
    '/app'

# Set run user and CMD
USER 'service'
CMD ["/app/service", "-d", "/app/data"]