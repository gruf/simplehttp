package main

import "html/template"

var (
	// dirIndexTplate is the default
	// dir index template definition.
	dirIndexTplate = `
<!DOCTYPE html>
<html>
<body>
<h2>Index {{.DirPath}}</h2>
<table>
	{{range .Files}}
	<tr style="width:120%">
		<td>{{.Mode.String}}</td>
		<td></td>
		<td><a href="{{.Path}}">{{.Name}}</a></td>
		<td></td>
		<td>{{.Size}}</td>
		<td></td>
		<td>{{.ModTime.Format "2006-01-02 15:04"}}</td>
	</tr>
	{{end}}
</table>
</body>
</html>
`
)

// ParseTemplate will parse a directory index template from given file
// path, or in case of empty path will return a default index template.
func ParseTemplate(path string) (*template.Template, error) {
	if path == "" {
		return template.New("default").Parse(dirIndexTplate)
	}
	return template.ParseFiles(path)
}
