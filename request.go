package main

import (
	"os"

	"codeberg.org/gruf/go-byteutil"
	"codeberg.org/gruf/go-fastpath/v2"
)

// FSRequest represents a filesystem request, with
// preallocated buffers for server operations.
type FSRequest struct {

	// stat contains the opened file's os.FileInfo.
	Stat os.FileInfo

	// file contains the opened file.
	File *os.File

	// Path is the calculated filesystem path.
	Path string

	// RawPath is the raw incoming path used to open this FSCtx
	RawPath string

	// Buf contains a byte buffer for file reads and template rendering.
	Buf byteutil.Buffer

	// PBuf is used for minimum alloc path building.
	PBuf fastpath.Builder
}

// NewFSRequest returns a new FSRequest based on buffer.
func NewFSRequest(bufsize int) *FSRequest {
	return &FSRequest{
		PBuf: fastpath.Builder{B: make([]byte, 0, 512)},
		Buf:  byteutil.Buffer{B: make([]byte, bufsize)},
	}
}

// Reset will reset the FSRequest instance
// variables, and close any opened file.
func (fsreq *FSRequest) Reset() {
	fsreq.File = nil
	fsreq.Stat = nil
	fsreq.Path = ""
	fsreq.RawPath = ""
	fsreq.PBuf.Reset()
	fsreq.Buf.Reset()
}
