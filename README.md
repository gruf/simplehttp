# simplehttp

a disgustingly simple HTTP(S) server

point it at a directory and away you go

```
Usage: ./simplehttp ...
 -a --accept-ranges (env: $ACCEPT_RANGES) (default: true)
    	Accept and handle range headers
 -b --buffer-size size (env: $BUFFER_SIZE) (default: 4096B)
    	File read and template execution buffer size
 -c --tls-cert string (env: $TLS_CERT)
    	Server TLS cert file
 -d --root string (env: $ROOT)
    	Root filesystem directory to serve
 -g --generate-index (env: $GENERATE_INDEX) (default: false)
    	Generate index page when no index files found
 -h --help
    	Print usage information
 -i --index-files []string (env: $INDEX_FILES)
    	Index files used in directory paths
 -k --tls-key string (env: $TLS_KEY)
    	Server TLS key file
 -l --listen string (env: $LISTEN) (default: 127.0.0.1:8080)
    	Server listen address
 -r --restricted []string (env: $RESTRICTED) (default: */.*)
    	Restricted filesystem path glob expressions
 -s --sort-by string (env: $SORT_BY) (default: name)
    	On index page generation, sort files by attribute (name,date,size)
 -t --index-template string (env: $INDEX_TEMPLATE)
    	Go HTML template path used to generate directory indices
 -u --auth-user []username:bcrypt_hash (env: $AUTH_USER)
    	Set permitted HTTP basic authentication users
 -v --version
    	Print version information
```

# building

to build locally: `scripts/build_local.sh`

to build docker image: `docker build .`

